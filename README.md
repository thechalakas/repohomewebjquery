# repohomewebjquery

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to jQuery

**here are the repos**

I have written some code related to jQuery. I mostly use it for training and sometimes for work. 

---

1. [https://bitbucket.org/thechalakas/jquery_using]

2. [https://bitbucket.org/thechalakas/phpbasics]

3. [https://bitbucket.org/thechalakas/jquery_add_remove]

4. [https://bitbucket.org/thechalakas/jquery_events]

5. [https://bitbucket.org/thechalakas/jquery_get_set]

6. [https://bitbucket.org/thechalakas/jquery_selectors]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 